package com.antoonvereecken.productserviceseureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class ProductServicesEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductServicesEurekaApplication.class, args);
	}

}